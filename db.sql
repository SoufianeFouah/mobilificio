DROP DATABASE IF EXISTS mobilificio;
create database mobilificio;
use mobilificio;

CREATE TABLE oggetto(
	oggettoId INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nome VARCHAR (150) NOT NULL,
    descrizione TEXT,  
    cod_ogg VARCHAR (150) UNIQUE,
    prezz_vend FLOAT NOT NULL,
    categorie VARCHAR (150)
);

CREATE TABLE categoria(
	categoriaId INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nome VARCHAR (150) NOT NULL,
    descrizione TEXT,  
    cod_cate VARCHAR (150) UNIQUE
);

INSERT INTO oggetto(nome, descrizione, cod_ogg, prezz_vend, categorie) VALUES
("Sedia", "Sedia pieghevole", "51451", 2.5, "ALTRO"),
("Tavolo", "Tavolo pieghevole", "51448", 3, "ALTRO"),
("Lampada", "Lampada bella forte", "514542", 25, "ALTRO");

INSERT INTO categoria(nome, descrizione, cod_cate) VALUES
("Cucina", "Per stare comodi", "51451"),
("Cucina", "Per mangiare", "51448"),
("Cucina", "Per la luce", "514542");

SELECT * FROM oggetto;
SELECT * FROM categoria;

