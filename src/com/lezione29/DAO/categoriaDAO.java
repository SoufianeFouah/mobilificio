package com.lezione29.DAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.lezione29.classi.categoria;
import com.lezione29.connessione.connettoreDB;
import com.mysql.jdbc.PreparedStatement;

public class categoriaDAO implements Dao<categoria>{
	
	public ArrayList<categoria> findAll() throws SQLException{
       	ArrayList<categoria> elenco = new ArrayList<categoria>();
       
   		Connection conn = connettoreDB.getIstanza().getConnessione();
           
       	String query = "SELECT categoriaId, nome, descrizione, cod_cate FROM categoria";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	
       	ResultSet risultato = ps.executeQuery();
       	while(risultato.next()){
       		categoria temp = new categoria();
       		temp.setCategoriaId(risultato.getInt(1));
       		temp.setNome(risultato.getString(2));
       		temp.setDescrizione(risultato.getString(3));
       		temp.setCod_cate(risultato.getFloat(4));
       		elenco.add(temp);
       	}
       	
       	return elenco;
	}

	@Override
	public categoria getById(int id) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<categoria> getAll() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void insert(categoria t) throws SQLException {
		// TODO Auto-generated method stub
		
	}

}
