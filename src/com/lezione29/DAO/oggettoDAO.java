package com.lezione29.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.lezione29.classi.oggetto;
import com.lezione29.connessione.connettoreDB;

public class oggettoDAO implements Dao<oggetto>{

	public ArrayList<oggetto> findAll() throws SQLException{
       	ArrayList<oggetto> elenco = new ArrayList<oggetto>();
       
   		Connection conn = connettoreDB.getIstanza().getConnessione();
           
       	String query = "SELECT oggettoId, nome, descrizione, cod_ogg, prezz_vend, categoria FROM oggetto";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	
       	ResultSet risultato = ps.executeQuery();
       	while(risultato.next()){
       		oggetto temp = new oggetto();
       		temp.setOggettoId(risultato.getInt(1));
       		temp.setNome(risultato.getString(2));
       		temp.setDescrizione(risultato.getString(3));
       		temp.setCod_ogg(risultato.getFloat(4));
       		temp.setPrezz_vend(risultato.getInt(5));
       		temp.setCategoria(risultato.getString(6));
       		elenco.add(temp);
       	}
       	
       	return elenco;
	}

	@Override
	public oggetto getById(int id) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<oggetto> getAll() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void insert(oggetto t) throws SQLException {
		// TODO Auto-generated method stub
		
	}
	
}
