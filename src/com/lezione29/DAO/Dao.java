package com.lezione29.DAO;

import java.sql.SQLException;
import java.util.ArrayList;

public interface Dao<T> {

	T getById(int id) throws SQLException;
	
	ArrayList<T> getAll() throws SQLException;
	
	void insert(T t) throws SQLException;
	
}
